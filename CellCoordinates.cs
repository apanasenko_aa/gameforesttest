﻿namespace Match_3
{
    class CellCoordinates
    {
        public int x { get; set; }
        public int y { get; set; }

        public CellCoordinates(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public CellCoordinates(CellCoordinates c)
        {
            this.x = c.x;
            this.y = c.y;
        }

        public static CellCoordinates operator + (CellCoordinates c1, CellCoordinates c2)
        {
            return new CellCoordinates(c1.x + c2.x, c1.y + c2.y);
        }

        public static CellCoordinates operator - (CellCoordinates c1, CellCoordinates c2)
        {
            return new CellCoordinates(c1.x - c2.x, c1.y - c2.y);
        }
    }
}
