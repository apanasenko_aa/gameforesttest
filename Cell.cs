﻿using System;
using Microsoft.Xna.Framework;

namespace Match_3
{
    class Cell
    {
        public enum Type { Red, Green, Blue, Orange, Purple, Yellow, Count };
        public enum Bonus { None, Vertical, Horizontal, Bomb, Count };

        private static Random random = new Random();

        private const double MIN_SIZE = 0.9;
        private const double MAX_SIZE = 1.1;
        private const double DECREMENT_STEP = 0.005;
        private const double MOVE_STEPS = 15; // 30 = 1 second
        private const double BOOM_SPEED = 0.05; // -n% in 1 iteration

        private CellCoordinates needCoords;
        private CellCoordinates speed;
        private bool decrement;

        public CellCoordinates curCoords { get; set; }
        public bool boom { get; set; }
        public Type type { get; set; }
        public Bonus bonus { get; set; }
        public double compressFactor { get; set; } // 1 - if not selected

        public Cell(CellCoordinates _curCoords, Type _type)
        {
            compressFactor = 1;
            decrement = false;
            curCoords = _curCoords;
            type = _type;
            needCoords = null;
            speed = null;
            boom = false;
            bonus = Bonus.None;
        }

        public Rectangle GetRectangle(int cellSize)
        {
            return new Rectangle(
                (int)(curCoords.x + (1 - compressFactor) * cellSize / 2.0),
                (int)(curCoords.y + (1 - compressFactor) * cellSize / 2.0),
                (int)(cellSize * compressFactor),
                (int)(cellSize * compressFactor)
            );
        }

        public void UpdateSize()
        {
            if (decrement)
            {
                compressFactor -= DECREMENT_STEP;
                decrement = compressFactor > MIN_SIZE;
            }
            else
            {
                compressFactor += DECREMENT_STEP;
                decrement = compressFactor > MAX_SIZE;
            }
        }

        public bool Move()
        {
            if (needCoords == null)
                return false;

            if (Math.Abs(needCoords.x - curCoords.x) <= Math.Abs(speed.x) && Math.Abs(needCoords.y - curCoords.y) <= Math.Abs(speed.y))
            {
                curCoords = needCoords;
                needCoords = null;
            }
            else
            {
                curCoords.x += speed.x;
                curCoords.y += speed.y;
            }
            return true;
        }

        public void SetNewCoords(CellCoordinates coors)
        {
            needCoords = new CellCoordinates(coors);
            speed = new CellCoordinates(
                (int)((needCoords.x - curCoords.x) / MOVE_STEPS),
                (int)((needCoords.y - curCoords.y) / MOVE_STEPS)
            );
        }

        public void SetNewCoords(int y)
        {
            needCoords = new CellCoordinates(curCoords);
            needCoords.y += y;
            speed = new CellCoordinates(
                (int)((needCoords.x - curCoords.x) / MOVE_STEPS),
                (int)((needCoords.y - curCoords.y) / MOVE_STEPS)
            );
        }

        public bool Boom()
        {
            compressFactor = Math.Max(compressFactor - BOOM_SPEED, 0);
            return compressFactor == 0;
        }

        public static Type GetRandomType()
        {
            return (Type)random.Next((int)Type.Count);
        }
    }
}
