﻿using Microsoft.Xna.Framework;

namespace Match_3
{
    class Destroyer
    {
        public enum Type { Up, Down, Left, Right, Count };

        private const double CELL_BY_TAKT = 0.05;
        private const int DELTA = 5;

        private int cellSize;
        private CellCoordinates speed;

        public CellCoordinates position { get; set; }
        public Type type { get; set; }

        public Destroyer(CellCoordinates _position, int _cellSize, Vector2 direction, Type _type)
        {
            position = _position;
            type = _type;
            cellSize = _cellSize;
            speed = new CellCoordinates(
                (int)(cellSize * CELL_BY_TAKT * direction.X),
                (int)(cellSize * CELL_BY_TAKT * direction.Y)
            );
        }

        public void Move()
        {
            position += speed;
        }

        public Rectangle GetRectangle()
        {
            return new Rectangle(position.x, position.y, cellSize, cellSize);
        }

        public bool InGameField(Rectangle r)
        {
            return r.Contains(position.x, position.y) || r.Contains(position.x + cellSize - DELTA, position.y + cellSize - DELTA);
        }
    }
}
