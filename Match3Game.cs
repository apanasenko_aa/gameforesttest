﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Match_3
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Match3Game : Game
    {
        enum GameStatus { Prepare, Unchecked, Boom, Moving, Checked, Select, MoveSelected, MoveBack, End };

        private const int FIELD_SIZE = 8;
        private const double INFOBAR_SIZE = 0.1;
        private const int WAIT_CLICK_MS = 80;

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        private CellCoordinates topLeftCorner;
        private GameStatus gameStatus;
        private Cell[,] gameField;
        private Cell selectedCell;
        private Cell newSelectedCell;
        private int cellSize;
        private int score;
        private DateTime endTime;
        private DateTime lastClick;
        private List<Destroyer> destroyerList;
        private List<Cell> boomList;

        // Texture
        private Texture2D playTexture;
        private Texture2D backgroundTexture;
        private Texture2D infobarTexture;
        private Texture2D gameOverTexture;
        private Texture2D okTexture;
        Dictionary<Cell.Type, List<Texture2D>> cellTextures;
        Dictionary<Destroyer.Type, Texture2D> destroyerTextures;

        private SpriteFont font;

        public Match3Game()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            IsMouseVisible = true;
            gameStatus = GameStatus.Prepare;
            selectedCell = null;
            newSelectedCell = null;
            lastClick = DateTime.Now;
            cellTextures = new Dictionary<Cell.Type, List<Texture2D>>();
            destroyerTextures = new Dictionary<Destroyer.Type, Texture2D>();
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            playTexture = Content.Load<Texture2D>("Images/play");
            backgroundTexture = Content.Load<Texture2D>("Images/background");
            infobarTexture = Content.Load<Texture2D>("Images/infobar");
            gameOverTexture = Content.Load<Texture2D>("Images/game_over");
            okTexture = Content.Load<Texture2D>("Images/ok");

            Cell.Type[] types = { Cell.Type.Red, Cell.Type.Green, Cell.Type.Blue, Cell.Type.Orange, Cell.Type.Purple, Cell.Type.Yellow };
            String[] names = { "red", "green", "blue", "orange", "purple", "yellow" };

            for (int i = 0; i < types.Length; i++)
            {
                cellTextures[types[i]] = new List<Texture2D>();
                for (int j = 1; j <= 4; j++)
                    cellTextures[types[i]].Add(Content.Load<Texture2D>("Images/" + names[i] + "_" + j));
            }

            destroyerTextures[Destroyer.Type.Up] = Content.Load<Texture2D>("Images/up");
            destroyerTextures[Destroyer.Type.Down] = Content.Load<Texture2D>("Images/down");
            destroyerTextures[Destroyer.Type.Left] = Content.Load<Texture2D>("Images/left");
            destroyerTextures[Destroyer.Type.Right] = Content.Load<Texture2D>("Images/right");

            font = Content.Load<SpriteFont>("font");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent() {}

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (gameStatus != GameStatus.Prepare && endTime.Subtract(DateTime.Now).Seconds <= 0)
               gameStatus = GameStatus.End;

            switch (gameStatus)
            {
                case GameStatus.Prepare:
                    MouseState state = Mouse.GetState();
                    if (state.LeftButton != ButtonState.Pressed)
                        break;

                    if (PlayButtonRectangle().Contains(state.X, state.Y))
                    {
                        InitializeGame();
                        gameStatus = GameStatus.Unchecked;
                    }
                    break;

                case GameStatus.Unchecked:
                    gameStatus = CheckAll()
                        ? GameStatus.Boom
                        : GameStatus.Checked;
                    break;

                case GameStatus.Checked:
                    selectedCell = GetCellByCoords(Mouse.GetState());
                    gameStatus = selectedCell == null
                        ? GameStatus.Checked
                        : GameStatus.Select;
                    break;

                case GameStatus.Select:
                    selectedCell.UpdateSize();

                    newSelectedCell = GetCellByCoords(Mouse.GetState());
                    if (newSelectedCell == null)
                        break;

                    int old_i = GetCellRow(selectedCell);
                    int old_j = GetCellCol(selectedCell);
                    int new_i = GetCellRow(newSelectedCell);
                    int new_j = GetCellCol(newSelectedCell);

                    int deff_i = Math.Abs(old_i - new_i);
                    int deff_j = Math.Abs(old_j - new_j);

                    selectedCell.compressFactor = 1.0;

                    if (deff_i != 1 && deff_j != 1 || deff_i + deff_j != 1)
                    {
                        selectedCell = null;
                        gameStatus = GameStatus.Checked;
                        break;
                    }

                    selectedCell.SetNewCoords(newSelectedCell.curCoords);
                    newSelectedCell.SetNewCoords(selectedCell.curCoords);

                    gameField[old_i, old_j] = newSelectedCell;
                    gameField[new_i, new_j] = selectedCell;

                    gameStatus = GameStatus.MoveSelected;
                    break;

                case GameStatus.MoveSelected:
                    bool m1 = selectedCell.Move();
                    bool m2 = newSelectedCell.Move();
                    if (m1 || m2)
                        break;

                    bool c1 = CheckArea(GetCellRow(selectedCell), GetCellCol(selectedCell));
                    bool c2 = CheckArea(GetCellRow(newSelectedCell), GetCellCol(newSelectedCell));
                    if (c1 || c2)
                    {
                        gameStatus = GameStatus.Boom;
                    }
                    else
                    {
                        selectedCell.SetNewCoords(newSelectedCell.curCoords);
                        newSelectedCell.SetNewCoords(selectedCell.curCoords);

                        gameField[GetCellRow(selectedCell), GetCellCol(selectedCell)] = newSelectedCell;
                        gameField[GetCellRow(newSelectedCell), GetCellCol(newSelectedCell)] = selectedCell;

                        gameStatus = GameStatus.MoveBack;
                    }
                    break;

                case GameStatus.MoveBack:
                    m1 = selectedCell.Move();
                    m2 = newSelectedCell.Move();
                    if (!m1 && !m2)
                        gameStatus = GameStatus.Checked;

                    break;

                case GameStatus.Boom:
                    bool allDead = true;
                    bool allDestroyerOutGame = false;
                    boomList.ForEach(x => allDead &= x.Boom());
                    for (int k = 0; k < destroyerList.Count; k++)
                    {
                        destroyerList[k].Move();
                        allDestroyerOutGame |= destroyerList[k].InGameField(new Rectangle(topLeftCorner.x, topLeftCorner.y, cellSize * FIELD_SIZE, cellSize * FIELD_SIZE));
                        int j = (destroyerList[k].position.x - topLeftCorner.x + cellSize / 2) / cellSize; // W
                        int i = (destroyerList[k].position.y - topLeftCorner.y + cellSize / 2) / cellSize; // H
                        BoomListAdd(i, j);
                    }
                    if (!allDead || allDestroyerOutGame)
                        break;

                    boomList.Clear();
                    destroyerList.Clear();
                    BoomCells();
                    gameStatus = GameStatus.Moving;
                    break;

                case GameStatus.Moving:
                    if (!MoveAll())
                        gameStatus = GameStatus.Unchecked;

                    break;

                case GameStatus.End:
                    state = Mouse.GetState();
                    if (state.LeftButton != ButtonState.Pressed)
                        break;

                    if (OkButtonRectangle().Contains(state.X, state.Y))
                        gameStatus = GameStatus.Prepare;

                    break;
            }
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            switch (gameStatus)
            {
                case GameStatus.Prepare:
                    DrawPrepareWindow();
                    break;

                default:
                    DrawGameWindow();
                    break;
            }

            base.Draw(gameTime);
        }

        private Rectangle PlayButtonRectangle()
        {
            int centX = Window.ClientBounds.Width / 2;
            int centY = Window.ClientBounds.Height / 2;
            int width = (int)(Window.ClientBounds.Width * 0.3);
            int height = (int)(Window.ClientBounds.Height * 0.15);
            return new Rectangle(
                centX - width / 2,
                centY - height / 2,
                width,
                height
            );
        }

        private Rectangle GameOverRectangle()
        {
            int centX = Window.ClientBounds.Width / 2;
            int centY = Window.ClientBounds.Height / 2;
            int width = (int)(Window.ClientBounds.Width * 0.4);
            int height = (int)(Window.ClientBounds.Height * 0.3);
            return new Rectangle(
                centX - width / 2,
                centY - height / 2,
                width,
                height
            );
        }

        private Rectangle OkButtonRectangle()
        {
            int centX = Window.ClientBounds.Width / 2;
            int centY = Window.ClientBounds.Height / 2;
            int width = (int)(Window.ClientBounds.Width * 0.1);
            return new Rectangle(
                centX - width / 2,
                centY,
                width,
                width
            );
        }

        private void DrawPrepareWindow()
        {
            GraphicsDevice.Clear(Color.White);
            spriteBatch.Begin();

            // background
            spriteBatch.Draw(
                backgroundTexture,
                new Rectangle(0, 0, Window.ClientBounds.Width, Window.ClientBounds.Height),
                Color.White
            );

            // playButton
            spriteBatch.Draw(
                playTexture,
                PlayButtonRectangle(),
                Color.White
            );
            spriteBatch.End();
        }

        private void DrawGameWindow()
        {
            GraphicsDevice.Clear(Color.Red);
            spriteBatch.Begin();

            // background
            spriteBatch.Draw(
                backgroundTexture,
                new Rectangle(0, 0, Window.ClientBounds.Width, Window.ClientBounds.Height),
                Color.White
            );

            // infobar
            spriteBatch.Draw(
                infobarTexture,
                new Rectangle(topLeftCorner.x, cellSize * FIELD_SIZE, cellSize * FIELD_SIZE, (int)(Window.ClientBounds.Height * INFOBAR_SIZE)),
                Color.White
            );

            Vector2 scopePosition = new Vector2(topLeftCorner.x + cellSize / 4, cellSize * FIELD_SIZE + cellSize / 4);
            spriteBatch.DrawString(font, "Score: " + score, scopePosition, Color.Black);

            Vector2 timePosition = new Vector2(topLeftCorner.x + cellSize / 4 + cellSize * FIELD_SIZE / 2, cellSize * FIELD_SIZE + cellSize / 4);
            spriteBatch.DrawString(font, "Time: " + Math.Max(0, endTime.Subtract(DateTime.Now).Seconds), timePosition, Color.Black);

            foreach (Cell cell in boomList)
                spriteBatch.Draw(
                        cellTextures[cell.type][(int)cell.bonus],
                        cell.GetRectangle(cellSize),
                        Color.White
                );

            for (int i = 0; i < FIELD_SIZE; i++)
                for (int j = 0; j < FIELD_SIZE; j++)
                    spriteBatch.Draw(
                        cellTextures[gameField[i, j].type][(int)gameField[i, j].bonus],
                        gameField[i, j].GetRectangle(cellSize),
                        Color.White
                    );

            foreach (Destroyer destroyer in destroyerList)
                spriteBatch.Draw(
                        destroyerTextures[destroyer.type],
                        destroyer.GetRectangle(),
                        Color.White
                );

            if (gameStatus == GameStatus.End)
            {
                spriteBatch.Draw(
                    gameOverTexture,
                    GameOverRectangle(),
                    Color.White
                );

                spriteBatch.Draw(
                    okTexture,
                    OkButtonRectangle(),
                    Color.White
                );
            }

            spriteBatch.End();
        }

        private void InitializeGame()
        {
            score = 0;
            endTime = DateTime.Now.AddMinutes(1);

            boomList = new List<Cell>();
            destroyerList = new List<Destroyer>();

            cellSize = (int)(Math.Min(Window.ClientBounds.Width, Window.ClientBounds.Height * (1 - INFOBAR_SIZE)) / FIELD_SIZE);
            topLeftCorner = new CellCoordinates(
                Window.ClientBounds.Width / 2 - cellSize * FIELD_SIZE / 2,
                0
            );

            CellCoordinates curCoords = new CellCoordinates(topLeftCorner);

            gameField = new Cell[FIELD_SIZE, FIELD_SIZE];
            for (int i = 0; i < FIELD_SIZE; i++)
            {
                for (int j = 0; j < FIELD_SIZE; j++)
                {
                    gameField[i, j] = new Cell(
                        curCoords,
                        Cell.GetRandomType()
                    );
                    curCoords += new CellCoordinates(cellSize, 0);
                }
                curCoords += new CellCoordinates(-cellSize * FIELD_SIZE, cellSize);
            }
        }

        private Cell GetCellByCoords(MouseState state)
        {
            if (lastClick.AddMilliseconds(WAIT_CLICK_MS) > DateTime.Now)
                return null;

            lastClick = DateTime.Now;

            if (state.LeftButton != ButtonState.Pressed)
                return null;

            int j = (state.X - topLeftCorner.x) / cellSize; // W
            int i = (state.Y - topLeftCorner.y) / cellSize; // H

            return 0 <= i && i < FIELD_SIZE && 0 <= j && j < FIELD_SIZE
                ? gameField[i, j]
                : null;
        }

        private int GetCellCol(Cell c)
        {
            return (c.curCoords.x - topLeftCorner.x) / cellSize;
        }

        private int GetCellRow(Cell c)
        {
            return (c.curCoords.y - topLeftCorner.y) / cellSize;
        }

        private Pair<bool, Cell.Bonus> CheckLine(int i, int j, bool isVertical)
        {
            Pair<bool, Cell.Bonus> result = new Pair<bool, Cell.Bonus>(false, Cell.Bonus.None);
            int delta_i = 0;
            int delta_j = 0;

            if (isVertical)
                delta_i = 1;
            else
                delta_j = 1;

            int first_i = i;
            int first_j = j;

            while(first_i >= 0 && first_j >= 0 && gameField[first_i, first_j].type == gameField[i, j].type)
            {
                first_i -= delta_i;
                first_j -= delta_j;
            }

            int last_i = i;
            int last_j = j;

            while (last_i < FIELD_SIZE && last_j < FIELD_SIZE && gameField[last_i, last_j].type == gameField[i, j].type)
            {
                last_i += delta_i;
                last_j += delta_j;
            }

            first_i += delta_i;
            first_j += delta_j;
            last_i -= delta_i;
            last_j -= delta_j;

            int count = Math.Max(last_i - first_i, last_j - first_j) + 1;
            if (count >= 3)
            {
                result.First = true;
                result.Second = count == 4
                    ? (isVertical ? Cell.Bonus.Vertical : Cell.Bonus.Horizontal)
                    : (count > 4 ? Cell.Bonus.Bomb : Cell.Bonus.None);
                IncrementScore(count);

                while (first_i <= last_i && first_j <= last_j)
                {
                    if (first_i != i || first_j != j)
                        BoomListAdd(first_i, first_j);
                    first_i += delta_i;
                    first_j += delta_j;
                }
            }

            return result;
        }

        private bool CheckArea(int i, int j)
        {
            Pair<bool, Cell.Bonus> verticalResult = CheckLine(i, j, true);
            Pair<bool, Cell.Bonus> horizontalResult = CheckLine(i, j, false);

            Cell.Bonus newBonus = (int)verticalResult.Second > (int)horizontalResult.Second
                ? verticalResult.Second
                : horizontalResult.Second;

            if (verticalResult.First || horizontalResult.First)
                if (gameField[i, j].bonus == Cell.Bonus.None && newBonus != Cell.Bonus.None)
                    gameField[i, j].bonus = newBonus;
                else
                    BoomListAdd(i, j);

            return verticalResult.First || horizontalResult.First;
        }

        private bool CheckLine(List<Pair<int, int>> line)
        {
            bool findMatch = false;
            int count = 1;

            for (int i = 1; i < line.Count; i++)
            {
                if (gameField[line[i].First, line[i].Second].type != gameField[line[i - 1].First, line[i - 1].Second].type)
                {
                    IncrementScore(count);
                    count = 0;
                }
                count += 1;

                if (count == 3)
                {
                    BoomListAdd(line[i].First, line[i].Second);
                    BoomListAdd(line[i - 1].First, line[i - 1].Second);
                    BoomListAdd(line[i - 2].First, line[i - 2].Second);
                    findMatch = true;
                }
                else if (count > 3)
                {
                    BoomListAdd(line[i].First, line[i].Second);
                }
            }

            return findMatch;
        }

        private bool CheckAll() // find all match and fill boomList, not create bonus
        {
            bool findMatch = false;

            List<Pair<int, int>> col = new List<Pair<int, int>>();
            List<Pair<int, int>> row = new List<Pair<int, int>>();

            for(int i = 0; i < FIELD_SIZE; i++)
            {
                for (int j = 0; j < FIELD_SIZE; j++)
                {
                    row.Add(new Pair<int, int>(i, j));
                    col.Add(new Pair<int, int>(j, i));
                }

                findMatch |= CheckLine(col);
                findMatch |= CheckLine(row);
                row.Clear();
                col.Clear();
            }

            return findMatch;
        }

        private void BoomCells()
        {
            for (int j = 0; j < FIELD_SIZE; j++)
            {
                int count = 0;
                for (int i = FIELD_SIZE - 1; i >= 0 ; i--)
                {
                    if (gameField[i, j].boom)
                    {
                        gameField[i, j] = null;
                        count++;
                    }
                    else if (count > 0)
                    {
                        gameField[i, j].SetNewCoords(count * cellSize);
                        gameField[i + count, j] = gameField[i, j];
                        gameField[i, j] = null;
                    }
                }
                for (int i = count - 1; i >= 0; i--)
                {
                    gameField[i, j] = new Cell(
                        new CellCoordinates(topLeftCorner.x + j * cellSize, (i - count) * cellSize),
                        Cell.GetRandomType()
                    );
                    gameField[i, j].SetNewCoords(count * cellSize);
                }
            }
        }

        private void BoomListAdd(int i, int j)
        {
            if (0 > i || i >= FIELD_SIZE || 0 > j || j >= FIELD_SIZE)
                return;

            if (gameField[i, j].boom)
                return;

            boomList.Add(gameField[i, j]);
            gameField[i, j].boom = true;
            IncrementScore(gameField[i, j].bonus);

            switch (gameField[i, j].bonus)
            {
                case Cell.Bonus.Vertical:
                    destroyerList.Add(new Destroyer(gameField[i, j].curCoords, cellSize, new Vector2(0, 1), Destroyer.Type.Down));
                    destroyerList.Add(new Destroyer(gameField[i, j].curCoords, cellSize, new Vector2(0, -1), Destroyer.Type.Up));
                    break;

                case Cell.Bonus.Horizontal:
                    destroyerList.Add(new Destroyer(gameField[i, j].curCoords, cellSize, new Vector2(1, 0), Destroyer.Type.Right));
                    destroyerList.Add(new Destroyer(gameField[i, j].curCoords, cellSize, new Vector2(-1, 0), Destroyer.Type.Left));
                    break;

                case Cell.Bonus.Bomb:
                    for (int ii = Math.Max(0, i - 1); ii <= Math.Min(i + 1, FIELD_SIZE - 1); ii++)
                        for (int jj = Math.Max(0, j - 1); jj <= Math.Min(j + 1, FIELD_SIZE - 1); jj++)
                            BoomListAdd(ii, jj);
                    break;
            }
        }

        private bool MoveAll()
        {
            bool isMoving = false;
            for (int i = 0; i < FIELD_SIZE; i++)
                for (int j = 0; j < FIELD_SIZE; j++)
                    isMoving |= gameField[i, j].Move();

            return isMoving;
        }

        private void IncrementScore(int count)
        {
            if (count >= 3)
                score += count * 10;
        }

        private void IncrementScore(Cell.Bonus bonus)
        {
            if (bonus == Cell.Bonus.Vertical)
                score += 501;
            else if (bonus == Cell.Bonus.Horizontal)
                score += 502;
            else if (bonus == Cell.Bonus.Bomb)
                score += 1003;
        }
    }
}
